/*
    Global Utility Function
    For export
*/
import {
    Alert
} from 'react-native';

// Variable
let globalNum = 300;

// Function
function addNumber(n1, n2) {

    return n1 + n2 + globalNum;
}

// Class - 一個物件的原形
class originClass {

    // 在此建立公用的變數
    constructor() {

    }

    functionA() {

    }
 
    functionB() {

    }
}

// 建立物件
let originObject = new originClass();

// 作為所有交通工具的父類別
class Vehical {

    // 在此建立公用的變數
    constructor() {
        this.passengers = [];
    }

    addPassenger(name) {

        let numA = 100;

        this.passengers.push(name);
    }
}

// 汽車類別
class Car extends Vehical {

    // 在此建立公用的變數
    constructor(model) {
        // 呼叫父類別的同名函數，此為建構子
        super();

        this.model = model;
    }
    
    showPassengersCount() {
        return this.passengers.length;
    }
}

// 飛機類別
class Airplane extends Vehical {

    // 在此建立公用的變數
    constructor(model, capacity) {
        // 呼叫父類別的同名函數，此為建構子
        super();

        this.model = model;
        this.capacity = capacity;
    }

    // override 父類別的方法
    addPassenger(name) {
        if (this.passengers.length < this.capacity) {
            super.addPassenger(name);
        }
        else {
            // 此為示範用，這樣寫並不好
            Alert.alert('Custom Full');
        }
    }

    showPassengersCount() {
        return this.passengers.length;
    }
}

class addNumberClass {

    constructor() {
        this.classNum = 100;
    }

    // Function in Class
    addNumberInClass(n1, n2) {

        let classNum = 1000;
        
        return n1 + n2 + this.classNum + classNum;
    }
}

// 一起在這兒做 Export
export {globalNum, addNumber, addNumberClass, Car, Airplane};
