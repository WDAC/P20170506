/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

// 導入特定的變數，function，class
import {
    globalNum,
    addNumber,
    addNumberClass,
    Car,
    Airplane
} from './src/sample';

export default class P20170506 extends Component {


  render() {

    // 無法直接設定 export 變數的值，要改為 setter 與 getter 的方式
    // globalNum = 300;
  
    let addNumberObject = new addNumberClass();
    let addNumber2 = new addNumberClass();

    // 汽車類別
    let myCar = new Car("BMW");
    myCar.addPassenger("John");
    myCar.addPassenger("Tom");

    // 飛機類別
    let myPlane = new Airplane('AirBus', 3);
    myPlane.addPassenger("User1");
    myPlane.addPassenger("User2");
    myPlane.addPassenger("User3");
    myPlane.addPassenger("User4");

    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit index.android.js
        </Text>
        <Text style={styles.instructions}>
          Double tap R on your keyboard to reload,{'\n'}
          Shake or press menu button for dev menu
        </Text>

        <Text style={styles.instructions}>
            globalNum = {globalNum + '\n'}
            addNumber = {addNumber(10, 100) + '\n'}

            addNumberObject = {addNumberObject.addNumberInClass(3000, 5000) + '\n'}
            addNumberObject2 = {addNumber2.addNumberInClass(100, 600) + '\n'}
            
            myCarModel = {myCar.model + '\n'}
            myCarPassengerCount = {myCar.showPassengersCount() + '\n'}

            myPlaneCount = {myPlane.showPassengersCount() + '\n'}
        </Text>
        
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('P20170506', () => P20170506);
